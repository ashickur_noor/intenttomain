package com.example.ashickurrahman.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button second;
    EditText etMain;
    public static  final int REQUEST_CODE=22;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        second = findViewById(R.id.startSecond);
        etMain= findViewById(R.id.name_text);

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, secondActivity.class);
                intent.putExtra("textValue",etMain.getText().toString());
                intent.putExtra("textValue2",etMain.getText().toString());
                startActivityForResult(intent,22);
            }

            //Onclick লিখলেই পরের অংশ চলে আসবে।
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //etMain= findViewById(R.id.name_text);
        etMain.setText("");
    }

    //
//    public void startSecond(View view) {
//        Intent intent = new Intent(MainActivity.this, secondActivity.class);
//        startActivity(intent);
//    }
}
