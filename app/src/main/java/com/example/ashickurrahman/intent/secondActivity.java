package com.example.ashickurrahman.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class secondActivity extends AppCompatActivity {

    TextView textView , textView2, textView3;
    Button button;
    EditText sendText;

    ArrayList<String> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView= findViewById(R.id.showtext);
        textView2= findViewById(R.id.showtext2);
        textView3= findViewById(R.id.showtext3);
        sendText=findViewById(R.id.sendtext);


        Bundle bundle=  getIntent().getExtras();
        //কোন অবজেক্ট তৈরি করার দরকার নাই। কারন শুধু ভেলু স্টোর করতেছি।
        textView.setText(bundle.get("textValue").toString());

        arrayList=(ArrayList<String>) bundle.get("arrayList");
        textView2.setText(arrayList.get(1));

        button=findViewById(R.id.second_send);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent();
                intent.putExtra("XYZ",sendText.getText().toString());
                setResult(MainActivity.REQUEST_CODE,intent);
                finish();

            }
        });

//        String AA=bundle.get("object").toString();
//        int val=Integer.parseInt(AA);
//
//        textView3.setText(val);










    }
}
